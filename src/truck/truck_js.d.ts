/* tslint:disable */
/* eslint-disable */
/**
* and operator
* @param {Solid} solid0
* @param {Solid} solid1
* @param {number | undefined} [tol]
* @returns {Solid | undefined}
*/
export function and(solid0: Solid, solid1: Solid, tol?: number): Solid | undefined;
/**
* or operator
* @param {Solid} solid0
* @param {Solid} solid1
* @param {number | undefined} [tol]
* @returns {Solid | undefined}
*/
export function or(solid0: Solid, solid1: Solid, tol?: number): Solid | undefined;
/**
* not operator
* @param {Solid} solid
* @returns {Solid}
*/
export function not(solid: Solid): Solid;
/**
* Creates and returns a vertex by a three dimensional point.
* @param {number} x
* @param {number} y
* @param {number} z
* @returns {Vertex}
*/
export function vertex(x: number, y: number, z: number): Vertex;
/**
* Returns a line from `vertex0` to `vertex1`.
* @param {Vertex} vertex0
* @param {Vertex} vertex1
* @returns {Edge}
*/
export function line(vertex0: Vertex, vertex1: Vertex): Edge;
/**
* Returns a circle arc from `vertex0` to `vertex1` via `transit`.
* @param {Vertex} vertex0
* @param {Vertex} vertex1
* @param {Float64Array} transit
* @returns {Edge}
*/
export function circle_arc(vertex0: Vertex, vertex1: Vertex, transit: Float64Array): Edge;
/**
* Returns a Bezier curve from `vertex0` to `vertex1` with inter control points `inter_points`.
* @param {Vertex} vertex0
* @param {Vertex} vertex1
* @param {Float64Array} inter_points
* @returns {Edge}
*/
export function bezier(vertex0: Vertex, vertex1: Vertex, inter_points: Float64Array): Edge;
/**
* Returns a homotopic face from `edge0` to `edge1`.
* @param {Edge} edge0
* @param {Edge} edge1
* @returns {Face}
*/
export function homotopy(edge0: Edge, edge1: Edge): Face;
/**
* Try attatiching a plane whose boundary is `wire`.
* @param {Wire} wire
* @returns {Face | undefined}
*/
export function try_attach_plane(wire: Wire): Face | undefined;
/**
* Returns a translated vertex, edge, wire, face, shell or solid.
* @param {AbstractShape} shape
* @param {Float64Array} vector
* @returns {AbstractShape}
*/
export function translated(shape: AbstractShape, vector: Float64Array): AbstractShape;
/**
* Returns a rotated vertex, edge, wire, face, shell or solid.
* @param {AbstractShape} shape
* @param {Float64Array} origin
* @param {Float64Array} axis
* @param {number} angle
* @returns {AbstractShape}
*/
export function rotated(shape: AbstractShape, origin: Float64Array, axis: Float64Array, angle: number): AbstractShape;
/**
* Returns a scaled vertex, edge, wire, face, shell or solid.
* @param {AbstractShape} shape
* @param {Float64Array} origin
* @param {Float64Array} scalars
* @returns {AbstractShape}
*/
export function scaled(shape: AbstractShape, origin: Float64Array, scalars: Float64Array): AbstractShape;
/**
* Sweeps a vertex, an edge, a wire, a face, or a shell by a vector.
* @param {AbstractShape} shape
* @param {Float64Array} vector
* @returns {AbstractShape}
*/
export function tsweep(shape: AbstractShape, vector: Float64Array): AbstractShape;
/**
* Sweeps a vertex, an edge, a wire, a face, or a shell by the rotation.
* @param {AbstractShape} shape
* @param {Float64Array} origin
* @param {Float64Array} axis
* @param {number} angle
* @returns {AbstractShape}
*/
export function rsweep(shape: AbstractShape, origin: Float64Array, axis: Float64Array, angle: number): AbstractShape;
/**
* STL type.
*/
export enum StlType {
/**
* Determine STL type automatically.
*
* # Reading
* If the first 5 bytes are..
* - "solid" => ascii format
* - otherwise => binary format
*
* # Writing
* Always binary format.
*/
  Automatic = 0,
/**
* ASCII format.
*/
  Ascii = 1,
/**
* Binary format.
*/
  Binary = 2,
}
/**
* abstract shape, effectively an enumerated type
*/
export class AbstractShape {
  free(): void;
/**
* check the type
* @returns {boolean}
*/
  is_vertex(): boolean;
/**
* downcast
* @returns {Vertex | undefined}
*/
  into_vertex(): Vertex | undefined;
/**
* check the type
* @returns {boolean}
*/
  is_edge(): boolean;
/**
* downcast
* @returns {Edge | undefined}
*/
  into_edge(): Edge | undefined;
/**
* check the type
* @returns {boolean}
*/
  is_wire(): boolean;
/**
* downcast
* @returns {Wire | undefined}
*/
  into_wire(): Wire | undefined;
/**
* check the type
* @returns {boolean}
*/
  is_face(): boolean;
/**
* downcast
* @returns {Face | undefined}
*/
  into_face(): Face | undefined;
/**
* check the type
* @returns {boolean}
*/
  is_shell(): boolean;
/**
* downcast
* @returns {Shell | undefined}
*/
  into_shell(): Shell | undefined;
/**
* check the type
* @returns {boolean}
*/
  is_solid(): boolean;
/**
* downcast
* @returns {Solid | undefined}
*/
  into_solid(): Solid | undefined;
}
/**
* wasm shape wrapper
*/
export class Edge {
  free(): void;
/**
* upcast to abstract shape
* @returns {AbstractShape}
*/
  upcast(): AbstractShape;
/**
* Gets the first vertex of the edge
* @returns {Vertex}
*/
  first_vertex(): Vertex;
/**
* Gets the second vertex of the edge
* @returns {Vertex}
*/
  second_vertex(): Vertex;
/**
* meshing edge
* @param {number} tol
* @returns {Float64Array}
*/
  to_polyline(tol: number): Float64Array;
}
/**
* wasm shape wrapper
*/
export class Face {
  free(): void;
/**
* upcast to abstract shape
* @returns {AbstractShape}
*/
  upcast(): AbstractShape;
/**
* Extract face as shell
* @returns {Shell}
*/
  extract(): Shell;
}
/**
* Buffer for rendering polygon
*/
export class PolygonBuffer {
  free(): void;
/**
* vertex buffer. One attribute contains `position: [f32; 3]`, `uv_coord: [f32; 2]` and `normal: [f32; 3]`.
* @returns {Float32Array}
*/
  vertex_buffer(): Float32Array;
/**
* the length (bytes) of vertex buffer. (Num of attributes) * 8 components * 4 bytes.
* @returns {number}
*/
  vertex_buffer_size(): number;
/**
* index buffer. `u32`.
* @returns {Uint32Array}
*/
  index_buffer(): Uint32Array;
/**
* the length (bytes) of index buffer. (Num of triangles) * 3 vertices * 4 bytes.
* @returns {number}
*/
  index_buffer_size(): number;
}
/**
* Wasm wrapper by Polygonmesh
*/
export class PolygonMesh {
  free(): void;
/**
* input from obj format
* @param {Uint8Array} data
* @returns {PolygonMesh | undefined}
*/
  static from_obj(data: Uint8Array): PolygonMesh | undefined;
/**
* input from STL format
* @param {Uint8Array} data
* @param {StlType} stl_type
* @returns {PolygonMesh | undefined}
*/
  static from_stl(data: Uint8Array, stl_type: StlType): PolygonMesh | undefined;
/**
* output obj format
* @returns {Uint8Array | undefined}
*/
  to_obj(): Uint8Array | undefined;
/**
* output stl format
* @param {StlType} stl_type
* @returns {Uint8Array | undefined}
*/
  to_stl(stl_type: StlType): Uint8Array | undefined;
/**
* Returns polygon buffer
* @returns {PolygonBuffer}
*/
  to_buffer(): PolygonBuffer;
/**
* meshing shell
* @param {Shell} shell
* @param {number} tol
* @returns {PolygonMesh}
*/
  static from_shell(shell: Shell, tol: number): PolygonMesh;
/**
* meshing solid
* @param {Solid} solid
* @param {number} tol
* @returns {PolygonMesh}
*/
  static from_solid(solid: Solid, tol: number): PolygonMesh;
/**
* Returns the bonding box
* @returns {Float64Array}
*/
  bounding_box(): Float64Array;
/**
* merge two polygons: `self` and `other`.
* @param {PolygonMesh} other
*/
  merge(other: PolygonMesh): void;
}
/**
* Shell and Solid parsed from step
*/
export class ShapeFromStep {
  free(): void;
/**
* meshing shape from step
* @param {number} tol
* @returns {PolygonMesh}
*/
  to_polygon(tol: number): PolygonMesh;
}
/**
* wasm shape wrapper
*/
export class Shell {
  free(): void;
/**
* upcast to abstract shape
* @returns {AbstractShape}
*/
  upcast(): AbstractShape;
/**
* meshing shape
* @param {number} tol
* @returns {PolygonMesh}
*/
  to_polygon(tol: number): PolygonMesh;
/**
* read shape from json
* @param {Uint8Array} data
* @returns {Shell | undefined}
*/
  static from_json(data: Uint8Array): Shell | undefined;
/**
* write shape to json
* @returns {Uint8Array}
*/
  to_json(): Uint8Array;
/**
* write shape to STEP
* @param {StepHeaderDescriptor} header
* @returns {string}
*/
  to_step(header: StepHeaderDescriptor): string;
/**
* faces of the shell
* @returns {(Face)[]}
*/
  faces(): (Face)[];
/**
* edges of the shell
* @returns {(Edge)[]}
*/
  edges(): (Edge)[];
/**
* vertices of the shell
* @returns {(Vertex)[]}
*/
  vertices(): (Vertex)[];
/**
* Creates Solid if `self` is a closed shell.
* @returns {Solid | undefined}
*/
  into_solid(): Solid | undefined;
}
/**
* wasm shape wrapper
*/
export class Solid {
  free(): void;
/**
* upcast to abstract shape
* @returns {AbstractShape}
*/
  upcast(): AbstractShape;
/**
* meshing shape
* @param {number} tol
* @returns {PolygonMesh}
*/
  to_polygon(tol: number): PolygonMesh;
/**
* read shape from json
* @param {Uint8Array} data
* @returns {Solid | undefined}
*/
  static from_json(data: Uint8Array): Solid | undefined;
/**
* write shape to json
* @returns {Uint8Array}
*/
  to_json(): Uint8Array;
/**
* write shape to STEP
* @param {StepHeaderDescriptor} header
* @returns {string}
*/
  to_step(header: StepHeaderDescriptor): string;
/**
* faces of the solid
* @returns {(Face)[]}
*/
  faces(): (Face)[];
/**
* edges of the solid
* @returns {(Edge)[]}
*/
  edges(): (Edge)[];
/**
* vertices of the solid
* @returns {(Vertex)[]}
*/
  vertices(): (Vertex)[];
}
/**
* Describe STEP file header
*/
export class StepHeaderDescriptor {
  free(): void;
/**
*/
  authorization: string;
/**
*/
  authors: (string)[];
/**
*/
  filename: string;
/**
*/
  organization: (string)[];
/**
*/
  organization_system: string;
/**
*/
  time_stamp: string;
}
/**
* step parse table
*/
export class Table {
  free(): void;
/**
* read step file
* @param {string} step_str
* @returns {Table | undefined}
*/
  static from_step(step_str: string): Table | undefined;
/**
* get shell indices
* @returns {BigUint64Array}
*/
  shell_indices(): BigUint64Array;
/**
* get shape from indices
* @param {bigint} idx
* @returns {ShapeFromStep | undefined}
*/
  get_shape(idx: bigint): ShapeFromStep | undefined;
}
/**
* wasm shape wrapper
*/
export class Vertex {
  free(): void;
/**
* upcast to abstract shape
* @returns {AbstractShape}
*/
  upcast(): AbstractShape;
/**
* vertex position
* @returns {Float64Array}
*/
  coordinates(): Float64Array;
}
/**
* wasm shape wrapper
*/
export class Wire {
  free(): void;
/**
* upcast to abstract shape
* @returns {AbstractShape}
*/
  upcast(): AbstractShape;
/**
* edges of the wire
* @returns {(Edge)[]}
*/
  edges(): (Edge)[];
/**
* vertices of the wire
* @returns {(Vertex)[]}
*/
  vertices(): (Vertex)[];
/**
* the wire as an polyline
* @param {number} tol
* @returns {Float64Array}
*/
  to_polyline(tol: number): Float64Array;
}

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly __wbg_vertex_free: (a: number) => void;
  readonly vertex_upcast: (a: number) => number;
  readonly __wbg_edge_free: (a: number) => void;
  readonly edge_upcast: (a: number) => number;
  readonly __wbg_wire_free: (a: number) => void;
  readonly wire_upcast: (a: number) => number;
  readonly __wbg_face_free: (a: number) => void;
  readonly face_upcast: (a: number) => number;
  readonly __wbg_shell_free: (a: number) => void;
  readonly shell_upcast: (a: number) => number;
  readonly __wbg_solid_free: (a: number) => void;
  readonly solid_upcast: (a: number) => number;
  readonly abstractshape_is_vertex: (a: number) => number;
  readonly abstractshape_into_vertex: (a: number) => number;
  readonly abstractshape_is_edge: (a: number) => number;
  readonly abstractshape_into_edge: (a: number) => number;
  readonly abstractshape_is_wire: (a: number) => number;
  readonly abstractshape_into_wire: (a: number) => number;
  readonly abstractshape_is_face: (a: number) => number;
  readonly abstractshape_into_face: (a: number) => number;
  readonly abstractshape_is_shell: (a: number) => number;
  readonly abstractshape_into_shell: (a: number) => number;
  readonly abstractshape_is_solid: (a: number) => number;
  readonly abstractshape_into_solid: (a: number) => number;
  readonly __wbg_abstractshape_free: (a: number) => void;
  readonly __wbg_stepheaderdescriptor_free: (a: number) => void;
  readonly stepheaderdescriptor_filename: (a: number) => number;
  readonly stepheaderdescriptor_set_filename: (a: number, b: number) => void;
  readonly stepheaderdescriptor_time_stamp: (a: number) => number;
  readonly stepheaderdescriptor_set_time_stamp: (a: number, b: number) => void;
  readonly stepheaderdescriptor_authors: (a: number, b: number) => void;
  readonly stepheaderdescriptor_set_authors: (a: number, b: number, c: number) => void;
  readonly stepheaderdescriptor_organization: (a: number, b: number) => void;
  readonly stepheaderdescriptor_set_organization: (a: number, b: number, c: number) => void;
  readonly stepheaderdescriptor_organization_system: (a: number) => number;
  readonly stepheaderdescriptor_set_organization_system: (a: number, b: number) => void;
  readonly stepheaderdescriptor_authorization: (a: number) => number;
  readonly stepheaderdescriptor_set_authorization: (a: number, b: number) => void;
  readonly shell_to_polygon: (a: number, b: number) => number;
  readonly shell_from_json: (a: number, b: number) => number;
  readonly shell_to_json: (a: number, b: number) => void;
  readonly shell_to_step: (a: number, b: number, c: number) => void;
  readonly solid_to_polygon: (a: number, b: number) => number;
  readonly solid_from_json: (a: number, b: number) => number;
  readonly solid_to_json: (a: number, b: number) => void;
  readonly solid_to_step: (a: number, b: number, c: number) => void;
  readonly solid_faces: (a: number, b: number) => void;
  readonly solid_edges: (a: number, b: number) => void;
  readonly solid_vertices: (a: number, b: number) => void;
  readonly shell_faces: (a: number, b: number) => void;
  readonly shell_edges: (a: number, b: number) => void;
  readonly shell_vertices: (a: number, b: number) => void;
  readonly shell_into_solid: (a: number) => number;
  readonly face_extract: (a: number) => number;
  readonly wire_edges: (a: number, b: number) => void;
  readonly wire_vertices: (a: number, b: number) => void;
  readonly wire_to_polyline: (a: number, b: number, c: number) => void;
  readonly edge_first_vertex: (a: number) => number;
  readonly edge_second_vertex: (a: number) => number;
  readonly edge_to_polyline: (a: number, b: number, c: number) => void;
  readonly vertex_coordinates: (a: number, b: number) => void;
  readonly __wbg_polygonmesh_free: (a: number) => void;
  readonly __wbg_polygonbuffer_free: (a: number) => void;
  readonly polygonmesh_from_obj: (a: number, b: number) => number;
  readonly polygonmesh_from_stl: (a: number, b: number, c: number) => number;
  readonly polygonmesh_to_obj: (a: number, b: number) => void;
  readonly polygonmesh_to_stl: (a: number, b: number, c: number) => void;
  readonly polygonmesh_to_buffer: (a: number) => number;
  readonly polygonmesh_from_shell: (a: number, b: number) => number;
  readonly polygonmesh_from_solid: (a: number, b: number) => number;
  readonly polygonmesh_bounding_box: (a: number, b: number) => void;
  readonly polygonmesh_merge: (a: number, b: number) => void;
  readonly polygonbuffer_vertex_buffer: (a: number, b: number) => void;
  readonly polygonbuffer_vertex_buffer_size: (a: number) => number;
  readonly polygonbuffer_index_buffer: (a: number, b: number) => void;
  readonly polygonbuffer_index_buffer_size: (a: number) => number;
  readonly and: (a: number, b: number, c: number, d: number) => number;
  readonly or: (a: number, b: number, c: number, d: number) => number;
  readonly not: (a: number) => number;
  readonly vertex: (a: number, b: number, c: number) => number;
  readonly line: (a: number, b: number) => number;
  readonly circle_arc: (a: number, b: number, c: number, d: number) => number;
  readonly bezier: (a: number, b: number, c: number, d: number) => number;
  readonly homotopy: (a: number, b: number) => number;
  readonly try_attach_plane: (a: number) => number;
  readonly translated: (a: number, b: number, c: number) => number;
  readonly rotated: (a: number, b: number, c: number, d: number, e: number, f: number) => number;
  readonly scaled: (a: number, b: number, c: number, d: number, e: number) => number;
  readonly tsweep: (a: number, b: number, c: number) => number;
  readonly rsweep: (a: number, b: number, c: number, d: number, e: number, f: number) => number;
  readonly __wbg_table_free: (a: number) => void;
  readonly __wbg_shapefromstep_free: (a: number) => void;
  readonly shapefromstep_to_polygon: (a: number, b: number) => number;
  readonly table_from_step: (a: number, b: number) => number;
  readonly table_shell_indices: (a: number, b: number) => void;
  readonly table_get_shape: (a: number, b: number) => number;
  readonly __wbindgen_malloc: (a: number, b: number) => number;
  readonly __wbindgen_realloc: (a: number, b: number, c: number, d: number) => number;
  readonly __wbindgen_add_to_stack_pointer: (a: number) => number;
  readonly __wbindgen_free: (a: number, b: number, c: number) => void;
}

export type SyncInitInput = BufferSource | WebAssembly.Module;
/**
* Instantiates the given `module`, which can either be bytes or
* a precompiled `WebAssembly.Module`.
*
* @param {SyncInitInput} module
*
* @returns {InitOutput}
*/
export function initSync(module: SyncInitInput): InitOutput;

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {InitInput | Promise<InitInput>} module_or_path
*
* @returns {Promise<InitOutput>}
*/
export default function __wbg_init (module_or_path?: InitInput | Promise<InitInput>): Promise<InitOutput>;
