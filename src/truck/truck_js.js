let wasm;

const heap = new Array(128).fill(undefined);

heap.push(undefined, null, true, false);

function getObject(idx) { return heap[idx]; }

let heap_next = heap.length;

function dropObject(idx) {
    if (idx < 132) return;
    heap[idx] = heap_next;
    heap_next = idx;
}

function takeObject(idx) {
    const ret = getObject(idx);
    dropObject(idx);
    return ret;
}

let WASM_VECTOR_LEN = 0;

let cachedUint8Memory0 = null;

function getUint8Memory0() {
    if (cachedUint8Memory0 === null || cachedUint8Memory0.byteLength === 0) {
        cachedUint8Memory0 = new Uint8Array(wasm.memory.buffer);
    }
    return cachedUint8Memory0;
}

const cachedTextEncoder = (typeof TextEncoder !== 'undefined' ? new TextEncoder('utf-8') : { encode: () => { throw Error('TextEncoder not available') } } );

const encodeString = (typeof cachedTextEncoder.encodeInto === 'function'
    ? function (arg, view) {
    return cachedTextEncoder.encodeInto(arg, view);
}
    : function (arg, view) {
    const buf = cachedTextEncoder.encode(arg);
    view.set(buf);
    return {
        read: arg.length,
        written: buf.length
    };
});

function passStringToWasm0(arg, malloc, realloc) {

    if (realloc === undefined) {
        const buf = cachedTextEncoder.encode(arg);
        const ptr = malloc(buf.length, 1) >>> 0;
        getUint8Memory0().subarray(ptr, ptr + buf.length).set(buf);
        WASM_VECTOR_LEN = buf.length;
        return ptr;
    }

    let len = arg.length;
    let ptr = malloc(len, 1) >>> 0;

    const mem = getUint8Memory0();

    let offset = 0;

    for (; offset < len; offset++) {
        const code = arg.charCodeAt(offset);
        if (code > 0x7F) break;
        mem[ptr + offset] = code;
    }

    if (offset !== len) {
        if (offset !== 0) {
            arg = arg.slice(offset);
        }
        ptr = realloc(ptr, len, len = offset + arg.length * 3, 1) >>> 0;
        const view = getUint8Memory0().subarray(ptr + offset, ptr + len);
        const ret = encodeString(arg, view);

        offset += ret.written;
        ptr = realloc(ptr, len, offset, 1) >>> 0;
    }

    WASM_VECTOR_LEN = offset;
    return ptr;
}

function isLikeNone(x) {
    return x === undefined || x === null;
}

let cachedInt32Memory0 = null;

function getInt32Memory0() {
    if (cachedInt32Memory0 === null || cachedInt32Memory0.byteLength === 0) {
        cachedInt32Memory0 = new Int32Array(wasm.memory.buffer);
    }
    return cachedInt32Memory0;
}

const cachedTextDecoder = (typeof TextDecoder !== 'undefined' ? new TextDecoder('utf-8', { ignoreBOM: true, fatal: true }) : { decode: () => { throw Error('TextDecoder not available') } } );

if (typeof TextDecoder !== 'undefined') { cachedTextDecoder.decode(); };

function getStringFromWasm0(ptr, len) {
    ptr = ptr >>> 0;
    return cachedTextDecoder.decode(getUint8Memory0().subarray(ptr, ptr + len));
}

function addHeapObject(obj) {
    if (heap_next === heap.length) heap.push(heap.length + 1);
    const idx = heap_next;
    heap_next = heap[idx];

    heap[idx] = obj;
    return idx;
}

let cachedUint32Memory0 = null;

function getUint32Memory0() {
    if (cachedUint32Memory0 === null || cachedUint32Memory0.byteLength === 0) {
        cachedUint32Memory0 = new Uint32Array(wasm.memory.buffer);
    }
    return cachedUint32Memory0;
}

function getArrayJsValueFromWasm0(ptr, len) {
    ptr = ptr >>> 0;
    const mem = getUint32Memory0();
    const slice = mem.subarray(ptr / 4, ptr / 4 + len);
    const result = [];
    for (let i = 0; i < slice.length; i++) {
        result.push(takeObject(slice[i]));
    }
    return result;
}

function passArrayJsValueToWasm0(array, malloc) {
    const ptr = malloc(array.length * 4, 4) >>> 0;
    const mem = getUint32Memory0();
    for (let i = 0; i < array.length; i++) {
        mem[ptr / 4 + i] = addHeapObject(array[i]);
    }
    WASM_VECTOR_LEN = array.length;
    return ptr;
}

function passArray8ToWasm0(arg, malloc) {
    const ptr = malloc(arg.length * 1, 1) >>> 0;
    getUint8Memory0().set(arg, ptr / 1);
    WASM_VECTOR_LEN = arg.length;
    return ptr;
}

function getArrayU8FromWasm0(ptr, len) {
    ptr = ptr >>> 0;
    return getUint8Memory0().subarray(ptr / 1, ptr / 1 + len);
}

function _assertClass(instance, klass) {
    if (!(instance instanceof klass)) {
        throw new Error(`expected instance of ${klass.name}`);
    }
    return instance.ptr;
}

let cachedFloat64Memory0 = null;

function getFloat64Memory0() {
    if (cachedFloat64Memory0 === null || cachedFloat64Memory0.byteLength === 0) {
        cachedFloat64Memory0 = new Float64Array(wasm.memory.buffer);
    }
    return cachedFloat64Memory0;
}

function getArrayF64FromWasm0(ptr, len) {
    ptr = ptr >>> 0;
    return getFloat64Memory0().subarray(ptr / 8, ptr / 8 + len);
}

let cachedFloat32Memory0 = null;

function getFloat32Memory0() {
    if (cachedFloat32Memory0 === null || cachedFloat32Memory0.byteLength === 0) {
        cachedFloat32Memory0 = new Float32Array(wasm.memory.buffer);
    }
    return cachedFloat32Memory0;
}

function getArrayF32FromWasm0(ptr, len) {
    ptr = ptr >>> 0;
    return getFloat32Memory0().subarray(ptr / 4, ptr / 4 + len);
}

function getArrayU32FromWasm0(ptr, len) {
    ptr = ptr >>> 0;
    return getUint32Memory0().subarray(ptr / 4, ptr / 4 + len);
}
/**
* and operator
* @param {Solid} solid0
* @param {Solid} solid1
* @param {number | undefined} [tol]
* @returns {Solid | undefined}
*/
export function and(solid0, solid1, tol) {
    _assertClass(solid0, Solid);
    _assertClass(solid1, Solid);
    const ret = wasm.and(solid0.__wbg_ptr, solid1.__wbg_ptr, !isLikeNone(tol), isLikeNone(tol) ? 0 : tol);
    return ret === 0 ? undefined : Solid.__wrap(ret);
}

/**
* or operator
* @param {Solid} solid0
* @param {Solid} solid1
* @param {number | undefined} [tol]
* @returns {Solid | undefined}
*/
export function or(solid0, solid1, tol) {
    _assertClass(solid0, Solid);
    _assertClass(solid1, Solid);
    const ret = wasm.or(solid0.__wbg_ptr, solid1.__wbg_ptr, !isLikeNone(tol), isLikeNone(tol) ? 0 : tol);
    return ret === 0 ? undefined : Solid.__wrap(ret);
}

/**
* not operator
* @param {Solid} solid
* @returns {Solid}
*/
export function not(solid) {
    _assertClass(solid, Solid);
    const ret = wasm.not(solid.__wbg_ptr);
    return Solid.__wrap(ret);
}

/**
* Creates and returns a vertex by a three dimensional point.
* @param {number} x
* @param {number} y
* @param {number} z
* @returns {Vertex}
*/
export function vertex(x, y, z) {
    const ret = wasm.vertex(x, y, z);
    return Vertex.__wrap(ret);
}

/**
* Returns a line from `vertex0` to `vertex1`.
* @param {Vertex} vertex0
* @param {Vertex} vertex1
* @returns {Edge}
*/
export function line(vertex0, vertex1) {
    _assertClass(vertex0, Vertex);
    _assertClass(vertex1, Vertex);
    const ret = wasm.line(vertex0.__wbg_ptr, vertex1.__wbg_ptr);
    return Edge.__wrap(ret);
}

function passArrayF64ToWasm0(arg, malloc) {
    const ptr = malloc(arg.length * 8, 8) >>> 0;
    getFloat64Memory0().set(arg, ptr / 8);
    WASM_VECTOR_LEN = arg.length;
    return ptr;
}
/**
* Returns a circle arc from `vertex0` to `vertex1` via `transit`.
* @param {Vertex} vertex0
* @param {Vertex} vertex1
* @param {Float64Array} transit
* @returns {Edge}
*/
export function circle_arc(vertex0, vertex1, transit) {
    _assertClass(vertex0, Vertex);
    _assertClass(vertex1, Vertex);
    const ptr0 = passArrayF64ToWasm0(transit, wasm.__wbindgen_malloc);
    const len0 = WASM_VECTOR_LEN;
    const ret = wasm.circle_arc(vertex0.__wbg_ptr, vertex1.__wbg_ptr, ptr0, len0);
    return Edge.__wrap(ret);
}

/**
* Returns a Bezier curve from `vertex0` to `vertex1` with inter control points `inter_points`.
* @param {Vertex} vertex0
* @param {Vertex} vertex1
* @param {Float64Array} inter_points
* @returns {Edge}
*/
export function bezier(vertex0, vertex1, inter_points) {
    _assertClass(vertex0, Vertex);
    _assertClass(vertex1, Vertex);
    const ptr0 = passArrayF64ToWasm0(inter_points, wasm.__wbindgen_malloc);
    const len0 = WASM_VECTOR_LEN;
    const ret = wasm.bezier(vertex0.__wbg_ptr, vertex1.__wbg_ptr, ptr0, len0);
    return Edge.__wrap(ret);
}

/**
* Returns a homotopic face from `edge0` to `edge1`.
* @param {Edge} edge0
* @param {Edge} edge1
* @returns {Face}
*/
export function homotopy(edge0, edge1) {
    _assertClass(edge0, Edge);
    _assertClass(edge1, Edge);
    const ret = wasm.homotopy(edge0.__wbg_ptr, edge1.__wbg_ptr);
    return Face.__wrap(ret);
}

/**
* Try attatiching a plane whose boundary is `wire`.
* @param {Wire} wire
* @returns {Face | undefined}
*/
export function try_attach_plane(wire) {
    _assertClass(wire, Wire);
    const ret = wasm.try_attach_plane(wire.__wbg_ptr);
    return ret === 0 ? undefined : Face.__wrap(ret);
}

/**
* Returns a translated vertex, edge, wire, face, shell or solid.
* @param {AbstractShape} shape
* @param {Float64Array} vector
* @returns {AbstractShape}
*/
export function translated(shape, vector) {
    _assertClass(shape, AbstractShape);
    const ptr0 = passArrayF64ToWasm0(vector, wasm.__wbindgen_malloc);
    const len0 = WASM_VECTOR_LEN;
    const ret = wasm.translated(shape.__wbg_ptr, ptr0, len0);
    return AbstractShape.__wrap(ret);
}

/**
* Returns a rotated vertex, edge, wire, face, shell or solid.
* @param {AbstractShape} shape
* @param {Float64Array} origin
* @param {Float64Array} axis
* @param {number} angle
* @returns {AbstractShape}
*/
export function rotated(shape, origin, axis, angle) {
    _assertClass(shape, AbstractShape);
    const ptr0 = passArrayF64ToWasm0(origin, wasm.__wbindgen_malloc);
    const len0 = WASM_VECTOR_LEN;
    const ptr1 = passArrayF64ToWasm0(axis, wasm.__wbindgen_malloc);
    const len1 = WASM_VECTOR_LEN;
    const ret = wasm.rotated(shape.__wbg_ptr, ptr0, len0, ptr1, len1, angle);
    return AbstractShape.__wrap(ret);
}

/**
* Returns a scaled vertex, edge, wire, face, shell or solid.
* @param {AbstractShape} shape
* @param {Float64Array} origin
* @param {Float64Array} scalars
* @returns {AbstractShape}
*/
export function scaled(shape, origin, scalars) {
    _assertClass(shape, AbstractShape);
    const ptr0 = passArrayF64ToWasm0(origin, wasm.__wbindgen_malloc);
    const len0 = WASM_VECTOR_LEN;
    const ptr1 = passArrayF64ToWasm0(scalars, wasm.__wbindgen_malloc);
    const len1 = WASM_VECTOR_LEN;
    const ret = wasm.scaled(shape.__wbg_ptr, ptr0, len0, ptr1, len1);
    return AbstractShape.__wrap(ret);
}

/**
* Sweeps a vertex, an edge, a wire, a face, or a shell by a vector.
* @param {AbstractShape} shape
* @param {Float64Array} vector
* @returns {AbstractShape}
*/
export function tsweep(shape, vector) {
    _assertClass(shape, AbstractShape);
    const ptr0 = passArrayF64ToWasm0(vector, wasm.__wbindgen_malloc);
    const len0 = WASM_VECTOR_LEN;
    const ret = wasm.tsweep(shape.__wbg_ptr, ptr0, len0);
    return AbstractShape.__wrap(ret);
}

/**
* Sweeps a vertex, an edge, a wire, a face, or a shell by the rotation.
* @param {AbstractShape} shape
* @param {Float64Array} origin
* @param {Float64Array} axis
* @param {number} angle
* @returns {AbstractShape}
*/
export function rsweep(shape, origin, axis, angle) {
    _assertClass(shape, AbstractShape);
    const ptr0 = passArrayF64ToWasm0(origin, wasm.__wbindgen_malloc);
    const len0 = WASM_VECTOR_LEN;
    const ptr1 = passArrayF64ToWasm0(axis, wasm.__wbindgen_malloc);
    const len1 = WASM_VECTOR_LEN;
    const ret = wasm.rsweep(shape.__wbg_ptr, ptr0, len0, ptr1, len1, angle);
    return AbstractShape.__wrap(ret);
}

let cachedBigUint64Memory0 = null;

function getBigUint64Memory0() {
    if (cachedBigUint64Memory0 === null || cachedBigUint64Memory0.byteLength === 0) {
        cachedBigUint64Memory0 = new BigUint64Array(wasm.memory.buffer);
    }
    return cachedBigUint64Memory0;
}

function getArrayU64FromWasm0(ptr, len) {
    ptr = ptr >>> 0;
    return getBigUint64Memory0().subarray(ptr / 8, ptr / 8 + len);
}
/**
* STL type.
*/
export const StlType = Object.freeze({
/**
* Determine STL type automatically.
*
* # Reading
* If the first 5 bytes are..
* - "solid" => ascii format
* - otherwise => binary format
*
* # Writing
* Always binary format.
*/
Automatic:0,"0":"Automatic",
/**
* ASCII format.
*/
Ascii:1,"1":"Ascii",
/**
* Binary format.
*/
Binary:2,"2":"Binary", });

const AbstractShapeFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_abstractshape_free(ptr >>> 0));
/**
* abstract shape, effectively an enumerated type
*/
export class AbstractShape {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(AbstractShape.prototype);
        obj.__wbg_ptr = ptr;
        AbstractShapeFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        AbstractShapeFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_abstractshape_free(ptr);
    }
    /**
    * check the type
    * @returns {boolean}
    */
    is_vertex() {
        const ret = wasm.abstractshape_is_vertex(this.__wbg_ptr);
        return ret !== 0;
    }
    /**
    * downcast
    * @returns {Vertex | undefined}
    */
    into_vertex() {
        const ptr = this.__destroy_into_raw();
        const ret = wasm.abstractshape_into_vertex(ptr);
        return ret === 0 ? undefined : Vertex.__wrap(ret);
    }
    /**
    * check the type
    * @returns {boolean}
    */
    is_edge() {
        const ret = wasm.abstractshape_is_edge(this.__wbg_ptr);
        return ret !== 0;
    }
    /**
    * downcast
    * @returns {Edge | undefined}
    */
    into_edge() {
        const ptr = this.__destroy_into_raw();
        const ret = wasm.abstractshape_into_edge(ptr);
        return ret === 0 ? undefined : Edge.__wrap(ret);
    }
    /**
    * check the type
    * @returns {boolean}
    */
    is_wire() {
        const ret = wasm.abstractshape_is_wire(this.__wbg_ptr);
        return ret !== 0;
    }
    /**
    * downcast
    * @returns {Wire | undefined}
    */
    into_wire() {
        const ptr = this.__destroy_into_raw();
        const ret = wasm.abstractshape_into_wire(ptr);
        return ret === 0 ? undefined : Wire.__wrap(ret);
    }
    /**
    * check the type
    * @returns {boolean}
    */
    is_face() {
        const ret = wasm.abstractshape_is_face(this.__wbg_ptr);
        return ret !== 0;
    }
    /**
    * downcast
    * @returns {Face | undefined}
    */
    into_face() {
        const ptr = this.__destroy_into_raw();
        const ret = wasm.abstractshape_into_face(ptr);
        return ret === 0 ? undefined : Face.__wrap(ret);
    }
    /**
    * check the type
    * @returns {boolean}
    */
    is_shell() {
        const ret = wasm.abstractshape_is_shell(this.__wbg_ptr);
        return ret !== 0;
    }
    /**
    * downcast
    * @returns {Shell | undefined}
    */
    into_shell() {
        const ptr = this.__destroy_into_raw();
        const ret = wasm.abstractshape_into_shell(ptr);
        return ret === 0 ? undefined : Shell.__wrap(ret);
    }
    /**
    * check the type
    * @returns {boolean}
    */
    is_solid() {
        const ret = wasm.abstractshape_is_solid(this.__wbg_ptr);
        return ret !== 0;
    }
    /**
    * downcast
    * @returns {Solid | undefined}
    */
    into_solid() {
        const ptr = this.__destroy_into_raw();
        const ret = wasm.abstractshape_into_solid(ptr);
        return ret === 0 ? undefined : Solid.__wrap(ret);
    }
}

const EdgeFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_edge_free(ptr >>> 0));
/**
* wasm shape wrapper
*/
export class Edge {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(Edge.prototype);
        obj.__wbg_ptr = ptr;
        EdgeFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        EdgeFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_edge_free(ptr);
    }
    /**
    * upcast to abstract shape
    * @returns {AbstractShape}
    */
    upcast() {
        const ptr = this.__destroy_into_raw();
        const ret = wasm.edge_upcast(ptr);
        return AbstractShape.__wrap(ret);
    }
    /**
    * Gets the first vertex of the edge
    * @returns {Vertex}
    */
    first_vertex() {
        const ret = wasm.edge_first_vertex(this.__wbg_ptr);
        return Vertex.__wrap(ret);
    }
    /**
    * Gets the second vertex of the edge
    * @returns {Vertex}
    */
    second_vertex() {
        const ret = wasm.edge_second_vertex(this.__wbg_ptr);
        return Vertex.__wrap(ret);
    }
    /**
    * meshing edge
    * @param {number} tol
    * @returns {Float64Array}
    */
    to_polyline(tol) {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.edge_to_polyline(retptr, this.__wbg_ptr, tol);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayF64FromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 8, 8);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
}

const FaceFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_face_free(ptr >>> 0));
/**
* wasm shape wrapper
*/
export class Face {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(Face.prototype);
        obj.__wbg_ptr = ptr;
        FaceFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        FaceFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_face_free(ptr);
    }
    /**
    * upcast to abstract shape
    * @returns {AbstractShape}
    */
    upcast() {
        const ptr = this.__destroy_into_raw();
        const ret = wasm.face_upcast(ptr);
        return AbstractShape.__wrap(ret);
    }
    /**
    * Extract face as shell
    * @returns {Shell}
    */
    extract() {
        const ptr = this.__destroy_into_raw();
        const ret = wasm.face_extract(ptr);
        return Shell.__wrap(ret);
    }
}

const PolygonBufferFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_polygonbuffer_free(ptr >>> 0));
/**
* Buffer for rendering polygon
*/
export class PolygonBuffer {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(PolygonBuffer.prototype);
        obj.__wbg_ptr = ptr;
        PolygonBufferFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        PolygonBufferFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_polygonbuffer_free(ptr);
    }
    /**
    * vertex buffer. One attribute contains `position: [f32; 3]`, `uv_coord: [f32; 2]` and `normal: [f32; 3]`.
    * @returns {Float32Array}
    */
    vertex_buffer() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.polygonbuffer_vertex_buffer(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayF32FromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 4, 4);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * the length (bytes) of vertex buffer. (Num of attributes) * 8 components * 4 bytes.
    * @returns {number}
    */
    vertex_buffer_size() {
        const ret = wasm.polygonbuffer_vertex_buffer_size(this.__wbg_ptr);
        return ret >>> 0;
    }
    /**
    * index buffer. `u32`.
    * @returns {Uint32Array}
    */
    index_buffer() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.polygonbuffer_index_buffer(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayU32FromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 4, 4);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * the length (bytes) of index buffer. (Num of triangles) * 3 vertices * 4 bytes.
    * @returns {number}
    */
    index_buffer_size() {
        const ret = wasm.polygonbuffer_index_buffer_size(this.__wbg_ptr);
        return ret >>> 0;
    }
}

const PolygonMeshFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_polygonmesh_free(ptr >>> 0));
/**
* Wasm wrapper by Polygonmesh
*/
export class PolygonMesh {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(PolygonMesh.prototype);
        obj.__wbg_ptr = ptr;
        PolygonMeshFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        PolygonMeshFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_polygonmesh_free(ptr);
    }
    /**
    * input from obj format
    * @param {Uint8Array} data
    * @returns {PolygonMesh | undefined}
    */
    static from_obj(data) {
        const ptr0 = passArray8ToWasm0(data, wasm.__wbindgen_malloc);
        const len0 = WASM_VECTOR_LEN;
        const ret = wasm.polygonmesh_from_obj(ptr0, len0);
        return ret === 0 ? undefined : PolygonMesh.__wrap(ret);
    }
    /**
    * input from STL format
    * @param {Uint8Array} data
    * @param {StlType} stl_type
    * @returns {PolygonMesh | undefined}
    */
    static from_stl(data, stl_type) {
        const ptr0 = passArray8ToWasm0(data, wasm.__wbindgen_malloc);
        const len0 = WASM_VECTOR_LEN;
        const ret = wasm.polygonmesh_from_stl(ptr0, len0, stl_type);
        return ret === 0 ? undefined : PolygonMesh.__wrap(ret);
    }
    /**
    * output obj format
    * @returns {Uint8Array | undefined}
    */
    to_obj() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.polygonmesh_to_obj(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            let v1;
            if (r0 !== 0) {
                v1 = getArrayU8FromWasm0(r0, r1).slice();
                wasm.__wbindgen_free(r0, r1 * 1, 1);
            }
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * output stl format
    * @param {StlType} stl_type
    * @returns {Uint8Array | undefined}
    */
    to_stl(stl_type) {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.polygonmesh_to_stl(retptr, this.__wbg_ptr, stl_type);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            let v1;
            if (r0 !== 0) {
                v1 = getArrayU8FromWasm0(r0, r1).slice();
                wasm.__wbindgen_free(r0, r1 * 1, 1);
            }
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * Returns polygon buffer
    * @returns {PolygonBuffer}
    */
    to_buffer() {
        const ret = wasm.polygonmesh_to_buffer(this.__wbg_ptr);
        return PolygonBuffer.__wrap(ret);
    }
    /**
    * meshing shell
    * @param {Shell} shell
    * @param {number} tol
    * @returns {PolygonMesh}
    */
    static from_shell(shell, tol) {
        _assertClass(shell, Shell);
        var ptr0 = shell.__destroy_into_raw();
        const ret = wasm.polygonmesh_from_shell(ptr0, tol);
        return PolygonMesh.__wrap(ret);
    }
    /**
    * meshing solid
    * @param {Solid} solid
    * @param {number} tol
    * @returns {PolygonMesh}
    */
    static from_solid(solid, tol) {
        _assertClass(solid, Solid);
        var ptr0 = solid.__destroy_into_raw();
        const ret = wasm.polygonmesh_from_solid(ptr0, tol);
        return PolygonMesh.__wrap(ret);
    }
    /**
    * Returns the bonding box
    * @returns {Float64Array}
    */
    bounding_box() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.polygonmesh_bounding_box(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayF64FromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 8, 8);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * merge two polygons: `self` and `other`.
    * @param {PolygonMesh} other
    */
    merge(other) {
        _assertClass(other, PolygonMesh);
        var ptr0 = other.__destroy_into_raw();
        wasm.polygonmesh_merge(this.__wbg_ptr, ptr0);
    }
}

const ShapeFromStepFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_shapefromstep_free(ptr >>> 0));
/**
* Shell and Solid parsed from step
*/
export class ShapeFromStep {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(ShapeFromStep.prototype);
        obj.__wbg_ptr = ptr;
        ShapeFromStepFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        ShapeFromStepFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_shapefromstep_free(ptr);
    }
    /**
    * meshing shape from step
    * @param {number} tol
    * @returns {PolygonMesh}
    */
    to_polygon(tol) {
        const ret = wasm.shapefromstep_to_polygon(this.__wbg_ptr, tol);
        return PolygonMesh.__wrap(ret);
    }
}

const ShellFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_shell_free(ptr >>> 0));
/**
* wasm shape wrapper
*/
export class Shell {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(Shell.prototype);
        obj.__wbg_ptr = ptr;
        ShellFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        ShellFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_shell_free(ptr);
    }
    /**
    * upcast to abstract shape
    * @returns {AbstractShape}
    */
    upcast() {
        const ptr = this.__destroy_into_raw();
        const ret = wasm.shell_upcast(ptr);
        return AbstractShape.__wrap(ret);
    }
    /**
    * meshing shape
    * @param {number} tol
    * @returns {PolygonMesh}
    */
    to_polygon(tol) {
        const ret = wasm.shell_to_polygon(this.__wbg_ptr, tol);
        return PolygonMesh.__wrap(ret);
    }
    /**
    * read shape from json
    * @param {Uint8Array} data
    * @returns {Shell | undefined}
    */
    static from_json(data) {
        const ptr0 = passArray8ToWasm0(data, wasm.__wbindgen_malloc);
        const len0 = WASM_VECTOR_LEN;
        const ret = wasm.shell_from_json(ptr0, len0);
        return ret === 0 ? undefined : Shell.__wrap(ret);
    }
    /**
    * write shape to json
    * @returns {Uint8Array}
    */
    to_json() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.shell_to_json(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayU8FromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 1, 1);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * write shape to STEP
    * @param {StepHeaderDescriptor} header
    * @returns {string}
    */
    to_step(header) {
        let deferred2_0;
        let deferred2_1;
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            _assertClass(header, StepHeaderDescriptor);
            var ptr0 = header.__destroy_into_raw();
            wasm.shell_to_step(retptr, this.__wbg_ptr, ptr0);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            deferred2_0 = r0;
            deferred2_1 = r1;
            return getStringFromWasm0(r0, r1);
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
            wasm.__wbindgen_free(deferred2_0, deferred2_1, 1);
        }
    }
    /**
    * faces of the shell
    * @returns {(Face)[]}
    */
    faces() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.shell_faces(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayJsValueFromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 4, 4);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * edges of the shell
    * @returns {(Edge)[]}
    */
    edges() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.shell_edges(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayJsValueFromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 4, 4);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * vertices of the shell
    * @returns {(Vertex)[]}
    */
    vertices() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.shell_vertices(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayJsValueFromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 4, 4);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * Creates Solid if `self` is a closed shell.
    * @returns {Solid | undefined}
    */
    into_solid() {
        const ptr = this.__destroy_into_raw();
        const ret = wasm.shell_into_solid(ptr);
        return ret === 0 ? undefined : Solid.__wrap(ret);
    }
}

const SolidFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_solid_free(ptr >>> 0));
/**
* wasm shape wrapper
*/
export class Solid {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(Solid.prototype);
        obj.__wbg_ptr = ptr;
        SolidFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        SolidFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_solid_free(ptr);
    }
    /**
    * upcast to abstract shape
    * @returns {AbstractShape}
    */
    upcast() {
        const ptr = this.__destroy_into_raw();
        const ret = wasm.solid_upcast(ptr);
        return AbstractShape.__wrap(ret);
    }
    /**
    * meshing shape
    * @param {number} tol
    * @returns {PolygonMesh}
    */
    to_polygon(tol) {
        const ret = wasm.solid_to_polygon(this.__wbg_ptr, tol);
        return PolygonMesh.__wrap(ret);
    }
    /**
    * read shape from json
    * @param {Uint8Array} data
    * @returns {Solid | undefined}
    */
    static from_json(data) {
        const ptr0 = passArray8ToWasm0(data, wasm.__wbindgen_malloc);
        const len0 = WASM_VECTOR_LEN;
        const ret = wasm.solid_from_json(ptr0, len0);
        return ret === 0 ? undefined : Solid.__wrap(ret);
    }
    /**
    * write shape to json
    * @returns {Uint8Array}
    */
    to_json() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.solid_to_json(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayU8FromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 1, 1);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * write shape to STEP
    * @param {StepHeaderDescriptor} header
    * @returns {string}
    */
    to_step(header) {
        let deferred2_0;
        let deferred2_1;
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            _assertClass(header, StepHeaderDescriptor);
            var ptr0 = header.__destroy_into_raw();
            wasm.solid_to_step(retptr, this.__wbg_ptr, ptr0);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            deferred2_0 = r0;
            deferred2_1 = r1;
            return getStringFromWasm0(r0, r1);
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
            wasm.__wbindgen_free(deferred2_0, deferred2_1, 1);
        }
    }
    /**
    * faces of the solid
    * @returns {(Face)[]}
    */
    faces() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.solid_faces(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayJsValueFromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 4, 4);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * edges of the solid
    * @returns {(Edge)[]}
    */
    edges() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.solid_edges(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayJsValueFromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 4, 4);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * vertices of the solid
    * @returns {(Vertex)[]}
    */
    vertices() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.solid_vertices(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayJsValueFromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 4, 4);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
}

const StepHeaderDescriptorFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_stepheaderdescriptor_free(ptr >>> 0));
/**
* Describe STEP file header
*/
export class StepHeaderDescriptor {

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        StepHeaderDescriptorFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_stepheaderdescriptor_free(ptr);
    }
    /**
    * @returns {string}
    */
    get filename() {
        const ret = wasm.stepheaderdescriptor_filename(this.__wbg_ptr);
        return takeObject(ret);
    }
    /**
    * @param {string} filename
    */
    set filename(filename) {
        wasm.stepheaderdescriptor_set_filename(this.__wbg_ptr, addHeapObject(filename));
    }
    /**
    * @returns {string}
    */
    get time_stamp() {
        const ret = wasm.stepheaderdescriptor_time_stamp(this.__wbg_ptr);
        return takeObject(ret);
    }
    /**
    * @param {string} time_stamp
    */
    set time_stamp(time_stamp) {
        wasm.stepheaderdescriptor_set_time_stamp(this.__wbg_ptr, addHeapObject(time_stamp));
    }
    /**
    * @returns {(string)[]}
    */
    get authors() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.stepheaderdescriptor_authors(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayJsValueFromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 4, 4);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {(string)[]} authors
    */
    set authors(authors) {
        const ptr0 = passArrayJsValueToWasm0(authors, wasm.__wbindgen_malloc);
        const len0 = WASM_VECTOR_LEN;
        wasm.stepheaderdescriptor_set_authors(this.__wbg_ptr, ptr0, len0);
    }
    /**
    * @returns {(string)[]}
    */
    get organization() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.stepheaderdescriptor_organization(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayJsValueFromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 4, 4);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {(string)[]} organization
    */
    set organization(organization) {
        const ptr0 = passArrayJsValueToWasm0(organization, wasm.__wbindgen_malloc);
        const len0 = WASM_VECTOR_LEN;
        wasm.stepheaderdescriptor_set_organization(this.__wbg_ptr, ptr0, len0);
    }
    /**
    * @returns {string}
    */
    get organization_system() {
        const ret = wasm.stepheaderdescriptor_organization_system(this.__wbg_ptr);
        return takeObject(ret);
    }
    /**
    * @param {string} organization_system
    */
    set organization_system(organization_system) {
        wasm.stepheaderdescriptor_set_organization_system(this.__wbg_ptr, addHeapObject(organization_system));
    }
    /**
    * @returns {string}
    */
    get authorization() {
        const ret = wasm.stepheaderdescriptor_authorization(this.__wbg_ptr);
        return takeObject(ret);
    }
    /**
    * @param {string} authorization
    */
    set authorization(authorization) {
        wasm.stepheaderdescriptor_set_authorization(this.__wbg_ptr, addHeapObject(authorization));
    }
}

const TableFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_table_free(ptr >>> 0));
/**
* step parse table
*/
export class Table {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(Table.prototype);
        obj.__wbg_ptr = ptr;
        TableFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        TableFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_table_free(ptr);
    }
    /**
    * read step file
    * @param {string} step_str
    * @returns {Table | undefined}
    */
    static from_step(step_str) {
        const ptr0 = passStringToWasm0(step_str, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        const len0 = WASM_VECTOR_LEN;
        const ret = wasm.table_from_step(ptr0, len0);
        return ret === 0 ? undefined : Table.__wrap(ret);
    }
    /**
    * get shell indices
    * @returns {BigUint64Array}
    */
    shell_indices() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.table_shell_indices(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayU64FromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 8, 8);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * get shape from indices
    * @param {bigint} idx
    * @returns {ShapeFromStep | undefined}
    */
    get_shape(idx) {
        const ret = wasm.table_get_shape(this.__wbg_ptr, idx);
        return ret === 0 ? undefined : ShapeFromStep.__wrap(ret);
    }
}

const VertexFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_vertex_free(ptr >>> 0));
/**
* wasm shape wrapper
*/
export class Vertex {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(Vertex.prototype);
        obj.__wbg_ptr = ptr;
        VertexFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        VertexFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_vertex_free(ptr);
    }
    /**
    * upcast to abstract shape
    * @returns {AbstractShape}
    */
    upcast() {
        const ptr = this.__destroy_into_raw();
        const ret = wasm.vertex_upcast(ptr);
        return AbstractShape.__wrap(ret);
    }
    /**
    * vertex position
    * @returns {Float64Array}
    */
    coordinates() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.vertex_coordinates(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayF64FromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 8, 8);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
}

const WireFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_wire_free(ptr >>> 0));
/**
* wasm shape wrapper
*/
export class Wire {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(Wire.prototype);
        obj.__wbg_ptr = ptr;
        WireFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        WireFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_wire_free(ptr);
    }
    /**
    * upcast to abstract shape
    * @returns {AbstractShape}
    */
    upcast() {
        const ptr = this.__destroy_into_raw();
        const ret = wasm.wire_upcast(ptr);
        return AbstractShape.__wrap(ret);
    }
    /**
    * edges of the wire
    * @returns {(Edge)[]}
    */
    edges() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.wire_edges(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayJsValueFromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 4, 4);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * vertices of the wire
    * @returns {(Vertex)[]}
    */
    vertices() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.wire_vertices(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayJsValueFromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 4, 4);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * the wire as an polyline
    * @param {number} tol
    * @returns {Float64Array}
    */
    to_polyline(tol) {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.wire_to_polyline(retptr, this.__wbg_ptr, tol);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayF64FromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 8, 8);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
}

async function __wbg_load(module, imports) {
    if (typeof Response === 'function' && module instanceof Response) {
        if (typeof WebAssembly.instantiateStreaming === 'function') {
            try {
                return await WebAssembly.instantiateStreaming(module, imports);

            } catch (e) {
                if (module.headers.get('Content-Type') != 'application/wasm') {
                    console.warn("`WebAssembly.instantiateStreaming` failed because your server does not serve wasm with `application/wasm` MIME type. Falling back to `WebAssembly.instantiate` which is slower. Original error:\n", e);

                } else {
                    throw e;
                }
            }
        }

        const bytes = await module.arrayBuffer();
        return await WebAssembly.instantiate(bytes, imports);

    } else {
        const instance = await WebAssembly.instantiate(module, imports);

        if (instance instanceof WebAssembly.Instance) {
            return { instance, module };

        } else {
            return instance;
        }
    }
}

function __wbg_get_imports() {
    const imports = {};
    imports.wbg = {};
    imports.wbg.__wbindgen_object_drop_ref = function(arg0) {
        takeObject(arg0);
    };
    imports.wbg.__wbindgen_string_get = function(arg0, arg1) {
        const obj = getObject(arg1);
        const ret = typeof(obj) === 'string' ? obj : undefined;
        var ptr1 = isLikeNone(ret) ? 0 : passStringToWasm0(ret, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        var len1 = WASM_VECTOR_LEN;
        getInt32Memory0()[arg0 / 4 + 1] = len1;
        getInt32Memory0()[arg0 / 4 + 0] = ptr1;
    };
    imports.wbg.__wbindgen_string_new = function(arg0, arg1) {
        const ret = getStringFromWasm0(arg0, arg1);
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_vertex_new = function(arg0) {
        const ret = Vertex.__wrap(arg0);
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_edge_new = function(arg0) {
        const ret = Edge.__wrap(arg0);
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_face_new = function(arg0) {
        const ret = Face.__wrap(arg0);
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_error_a526fb08a0205972 = function(arg0, arg1) {
        var v0 = getArrayJsValueFromWasm0(arg0, arg1).slice();
        wasm.__wbindgen_free(arg0, arg1 * 4, 4);
        console.error(...v0);
    };
    imports.wbg.__wbindgen_throw = function(arg0, arg1) {
        throw new Error(getStringFromWasm0(arg0, arg1));
    };

    return imports;
}

function __wbg_init_memory(imports, maybe_memory) {

}

function __wbg_finalize_init(instance, module) {
    wasm = instance.exports;
    __wbg_init.__wbindgen_wasm_module = module;
    cachedBigUint64Memory0 = null;
    cachedFloat32Memory0 = null;
    cachedFloat64Memory0 = null;
    cachedInt32Memory0 = null;
    cachedUint32Memory0 = null;
    cachedUint8Memory0 = null;


    return wasm;
}

function initSync(module) {
    if (wasm !== undefined) return wasm;

    const imports = __wbg_get_imports();

    __wbg_init_memory(imports);

    if (!(module instanceof WebAssembly.Module)) {
        module = new WebAssembly.Module(module);
    }

    const instance = new WebAssembly.Instance(module, imports);

    return __wbg_finalize_init(instance, module);
}

async function __wbg_init(input) {
    if (wasm !== undefined) return wasm;

    if (typeof input === 'undefined') {
        input = new URL('truck_js_bg.wasm', import.meta.url);
    }
    const imports = __wbg_get_imports();

    if (typeof input === 'string' || (typeof Request === 'function' && input instanceof Request) || (typeof URL === 'function' && input instanceof URL)) {
        input = fetch(input);
    }

    __wbg_init_memory(imports);

    const { instance, module } = await __wbg_load(await input, imports);

    return __wbg_finalize_init(instance, module);
}

export { initSync }
export default __wbg_init;
