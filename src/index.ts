// Typescript allow implicit any type
// @ts-nocheck

import * as monaco from 'monaco-editor'
import editorWorker from 'monaco-editor/esm/vs/editor/editor.worker?worker'
import tsWorker from 'monaco-editor/esm/vs/language/typescript/ts.worker?worker'

import { ThreeViewport } from './threeViewport'

import BoxExample from './demos/Box.ts.demo?raw'
import HammerExample from './demos/Hammer.ts.demo?raw'
import MinitruckExample from './demos/Minitruck.ts.demo?raw'
import Circle from './demos/Circle.ts.demo?raw'

self.MonacoEnvironment = {
  getWorker(_, label) {
    if (label === 'typescript' || label === 'javascript') {
      return new tsWorker()
    }
    return new editorWorker()
  }
}

import init from "./truck/truck_js";
import * as Truck from "./truck/truck_js";
import truck_js_dts from "./truck/truck_js.d.ts?raw";

const examples = {
  "box": BoxExample,
  "hammer": HammerExample,
  "minitruck": MinitruckExample,
  "circle": Circle,
}

if (document.readyState !== "loading") {
  onLoad();
} else {
  addEventListener("load", onLoad, false);
}

// let viewport!: WebGLViewport;
let viewport!: ThreeViewport;

let loaded = false;
async function onLoad() {

  // Bind event handlers
  document.getElementById("select-example").addEventListener("change", loadExample);

  document.getElementsByClassName("command run")[0].addEventListener("click", run);
  document.getElementsByClassName("command save-code")[0].addEventListener("click", downloadCode);
  document.getElementsByClassName("command save-json")[0].addEventListener("click", downloadJSON);
  document.getElementsByClassName("command save-step")[0].addEventListener("click", downloadStep);
  document.getElementsByClassName("command save-obj")[0].addEventListener("click", downloadObj);
  document.getElementsByClassName("command open")[0].addEventListener("click", openCode);

  // Editor
  setupEditor();

  await init();

  // init viewport
  const host = document.getElementById("viewport")!;
  viewport = new ThreeViewport(host);
  await viewport.init();

  // Truck
  await run();
}

function loadExample(e) {
  const example = examples[e.target.value];
  monaco.editor.getModels()[0].setValue(example);
  run();
}

function getCode() {
  return monaco.editor.getModels()[0].getValue();
}

async function run() {
  const root = document.getElementsByTagName("html")[0];
  root.setAttribute("state", "running")

  // allow interupt to update UI, 100 miliseconds ensures codes gets upated in editor with syntax highlighting
  await new Promise(resolve => setTimeout(resolve, 100));

  // get code from monaco editor
  let editorCode = getCode();
  if (editorCode.trim() == "") {
    return;
  }

  let code = `
    ${getCode()}
    return run();
  `

  let geometry = await execute(code);

  viewport.updateGeometry(geometry);
  loaded = true;

  root.setAttribute("state", "ok")
}

async function execute(code) {
  let solids: Truck.Solid[] = [];
  // create a function with one input argument from a string
  const msg = "Oh, no something is wrong with the code 😖";
  try {
    let modeling_function = new Function("Truck", code);
    solids = modeling_function(Truck);

    if (solids[0] === undefined) {
      throw new Error(msg);
    }
  } catch (e) {
    console.error(e);
    alert(msg+"\n"+e.message);
    root.setAttribute("state", "error")
    return;
  }

  return solids;
}

function setupEditor() {
  // Editor
  let editorContainer = document.getElementById("editor")!;

  // Typescript/Javascript settings
  monaco.languages.typescript.javascriptDefaults.setDiagnosticsOptions({
    noSemanticValidation: false,
    noSyntaxValidation: false,
  });

  monaco.languages.typescript.javascriptDefaults.setCompilerOptions({
    target: monaco.languages.typescript.ScriptTarget.ES2016,
    allowNonTsExtensions: true,
  });

  let editor = monaco.editor.create(editorContainer!, {
    value: examples["box"],
    automaticLayout: true,
    language: "typescript",
    theme: "vs-dark",
    scrollBeyondLastLine: false,
    minimap: {
      enabled: false,
    },
    lightbulb: {
      enabled: true,
    },
  });

  // Keybindings
  editor.addCommand(monaco.KeyMod.CtrlCmd | monaco.KeyMod.Shift | monaco.KeyCode.KeyB, run);

  // Prevent window defaults
  window.addEventListener("keydown", function (e) {
    if (e.ctrlKey && e.key === "s") {
      e.preventDefault();
      downloadCode(e);
    }
     if (e.ctrlKey && e.key === "o") {
      e.preventDefault();
      openCode(e);
     }
  });

  // Additional modules
  let libSource = "declare module Truck {\n" + truck_js_dts + "\n}";
  var libUri = "truck.d.ts";
  monaco.languages.typescript.typescriptDefaults.addExtraLib(libSource, libUri);
}

function fileRead(e) {
  e.preventDefault();
  const file0 = this.files[0];
  if (typeof file0 === "undefined") {
    console.warn("invalid input");
    return;
  }
  console.log(file0.name);
  const file_ext = file0.name.split(".").pop();

  const reader = new FileReader();
  reader.readAsArrayBuffer(file0);
  reader.onload = function () {
    const result = new Uint8Array(reader.result);

    let shape;
    if (file_ext === "json") {
      shape = Truck.Solid.from_json(result);
      if (typeof shape === "undefined") {
        console.warn("invalid json");
        return;
      }
    } else if (file_ext === "step" || file_ext === "stp") {
      const step_str = String.fromCharCode(...result);
      const table = Truck.Table.from_step(step_str);
      const indices = table.shell_indices();
      shape = table.get_shape(indices[0]);
      if (typeof shape === "undefined") {
        console.warn("invalid step");
        return;
      }
    }
    polygons = shape.to_polygon(0.01);
    if (typeof polygons === "undefined") {
      console.warn("meshing failed");
      return;
    }
    const box = polygons.bounding_box();
    const scale = Math.max(
      box[3] - box[0],
      box[4] - box[1],
      box[5] - box[2],
    ) * 2.0;
    const boxCenter = [
      (box[0] + box[3]) / 2.0,
      (box[1] + box[4]) / 2.0,
      (box[2] + box[5]) / 2.0,
    ];
    cameraPosition = [boxCenter[0], boxCenter[1], scale + boxCenter[2]];
    cameraDirection = [0.0, 0.0, -1.0];
    cameraUpdirection = [0.0, 1.0, 0.0];
    cameraGaze = boxCenter;
    cameraGaze = boxCenter;
    const object = polygons.to_buffer();
    vBuffer = object.vertex_buffer();
    iBuffer = object.index_buffer();
    indexLength = object.index_buffer_size() / 4;
    loaded = true;
  };
}

function openCode(e) {
  var input = document.createElement('input');
  input.type = 'file';
  input.accept = '.ts';

  input.onchange = e => {
     // getting a hold of the file reference
     var file = e.target.files[0];

     // setting up the reader
     var reader = new FileReader();
     reader.readAsText(file,'UTF-8');

     // here we tell the reader what to do when it's done reading...
     reader.onload = readerEvent => {
        var content = readerEvent.target.result; // this is the content!
        monaco.editor.getModels()[0].setValue(content);
     }
  }

  input.click();
}

function downloadCode(e) {
  e.preventDefault();
  const code = getCode();

  const blob = new Blob([code], {
    type: "text/plain",
  });

  const url = URL.createObjectURL(blob);
  const a = document.createElement("a");
  document.body.appendChild(a);
  a.download = "code.ts";
  a.href = url;
  a.click();
  a.remove();
  URL.revokeObjectURL(url);
}

function downloadJSON(e) {
  e.preventDefault();
  const json = renderableObjects[0].geometry.to_json();

  const blob = new Blob([json], {
    type: "text/plain",
  });

  const url = URL.createObjectURL(blob);
  const a = document.createElement("a");
  document.body.appendChild(a);
  a.download = "MiniTruckPart.mtp";
  a.href = url;
  a.click();
  a.remove();
  URL.revokeObjectURL(url);
}

function downloadStep(e) {
  e.preventDefault();
  const filename = "MiniTruckPart.step";

  const header = Truck.StepHeaderDescriptor.create();
  header.authors = "Created with MiniTruck!"; // 🛻🗯️";
  header.filename = filename;
  header.time_stamp = new Date().toISOString();

  const step = renderableObjects[0].geometry.to_step(header);

  const blob = new Blob([step], {
    type: "text/plain",
  });

  const url = URL.createObjectURL(blob);
  const a = document.createElement("a");
  document.body.appendChild(a);
  a.download = filename;
  a.href = url;
  a.click();
  a.remove();
  URL.revokeObjectURL(url);
}

function downloadObj(e) {
  const obj = renderableObjects[0].polygon.to_obj();
  if (typeof obj === "undefined") {
    console.warn("Failed to generate obj.");
    return;
  }
  const blob = new Blob([(new TextDecoder()).decode(obj)], {
    type: "text/plain",
  });
  const url = URL.createObjectURL(blob);
  const a = document.createElement("a");
  document.body.appendChild(a);
  a.download = "meshdata.obj";
  a.href = url;
  a.click();
  a.remove();
  URL.revokeObjectURL(url);
}
