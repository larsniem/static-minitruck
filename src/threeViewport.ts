import * as THREE from 'three';
import { OrbitControls } from "three/addons/controls/OrbitControls.js";

import * as Truck from "./truck/truck_js";


export class ThreeViewport {
  constructor(host: HTMLCanvasElement) {
    this.host = host;
  }

  host!: HTMLCanvasElement;

  renderer!: THREE.WebGLRenderer;

  scene!: THREE.Scene;
  camera!: THREE.PerspectiveCamera;
  controls!: OrbitControls;

  factor: number = 100;

  async init() {
    const canvas = this.host;

    this.renderer = new THREE.WebGLRenderer({ antialias: true, canvas });
    this.renderer.shadowMap.enabled = true;
    this.renderer.setSize(canvas.clientWidth, canvas.clientHeight, false);

    const camera = new THREE.PerspectiveCamera(
      50,
      canvas.clientWidth / canvas.clientHeight,
      0.1,
      1000
    );
    const factor = 100;
    camera.position.x = factor;
    camera.position.y = factor;
    camera.position.z = factor;
    camera.lookAt(0, 0, 0);
    
    this.camera = camera;
    
    window.addEventListener("resize",
      () => this.debounce(500, () => this.resizeRendererToDisplaySize())
    );

    this.controls = new OrbitControls(camera, canvas);
    this.controls.update();

    this.createScene();

    requestAnimationFrame(() => this.animate());
  }

  debouncing = false;
  debounce(timeout: number, func: any) {
    if (this.debouncing) {
      return;
    }
    this.debouncing = true;
    setTimeout(() => {
      this.debouncing = false
      func();
    }
    , timeout);
  }

   resizeRendererToDisplaySize() {
    const renderer = this.renderer;
    
    const canvas = this.renderer.domElement as HTMLCanvasElement;
    const container = canvas.parentElement as HTMLElement;

    // const width = canvas.clientWidth;
    // const height = canvas.clientHeight;
    const width = container.clientWidth;
    const height = container.clientHeight;

    const needResize = canvas.width !== width || canvas.height !== height;
    console.log(`Width (is/should): ${renderer.getSize(new THREE.Vector2()).width}/${width}`);
    console.log(`Height (is/should): ${renderer.getSize(new THREE.Vector2()).height}/${height}`);

    if (needResize) {
      renderer.setSize(width, height, false);
      this.camera.aspect = canvas.clientWidth / canvas.clientHeight;
      this.camera.updateProjectionMatrix();
      this.camera.zoom = 1;
    }

    return needResize;
  }

  updateGeometry(geometry: Object[]) {
    const scene = this.scene;

    const partGroup = scene.getObjectByName("part") as THREE.Group;
    partGroup.clear();

    const solids = geometry.filter((g) => g instanceof Truck.Solid) as Truck.Solid[];
    const wires = geometry.filter((g) => g instanceof Truck.Wire) as Truck.Wire[];

    for (let solid of solids) {
      // const geometries = this.createGeometry(solid);
      const geometries = this.createGeometryPerFace(solid);

      for (let geometry of geometries) {
        const color = new THREE.Color(0xfffff * Math.random());

        const material = new THREE.MeshPhongMaterial({
          color: color,
          side: THREE.DoubleSide,
          flatShading: true,
        });
  
        const mesh = new THREE.Mesh(geometry, material);
        mesh.castShadow = true;
        mesh.receiveShadow = true;
  
        partGroup.add(mesh);
  
        const vertices = solids[0].vertices();
        for (const vertex of vertices) {
          const position = vertex.coordinates();
          const sphereGeometry = new THREE.SphereGeometry(0.01, 32, 32);
          const sphereMaterial = new THREE.MeshBasicMaterial({ color: 0xffffff });
          const sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
  
          sphere.position.set(position[0], position[1], position[2]);
          partGroup.add(sphere);
        }
      }
    }

    for (let wire of wires) {
      const geometry = new THREE.BufferGeometry();
      
      let positions = wire.to_polyline(0.01);
      
      // const positionBuffer = new Float32Array(positions.length * 3);
      // for (let i = 0; i < positions.length; i++) {
      //   positionBuffer[i * 3] = positions[i][0];
      //   positionBuffer[i * 3 + 1] = positions[i][1];
      //   positionBuffer[i * 3 + 2] = positions[i][2];
      // }
      
      const positionBuffer = new Float32Array(positions);
      geometry.setAttribute("position", new THREE.BufferAttribute(positionBuffer, 3));
      const material = new THREE.LineBasicMaterial({ color: 0x000000 });
      const line = new THREE.Line(geometry, material);
      partGroup.add(line);
    }
  }

  createGeometry(solid: Truck.Solid): THREE.BufferGeometry[] {
    let polygons: Truck.PolygonMesh = solid.to_polygon(0.01);
    let buffer = polygons.to_buffer();

    const vertexBuffer = buffer.vertex_buffer();

    const positionComponentSize = 3;
    const normalComponentSize = 3;
    const uvComponentSize = 2;

    let vertexCount = vertexBuffer.length / (positionComponentSize + normalComponentSize + uvComponentSize);
    
    const positionBuffer = new Float32Array(vertexCount * positionComponentSize);
    const uvBuffer = new Float32Array(vertexCount * uvComponentSize);
    const normalBuffer = new Float32Array(vertexCount * normalComponentSize);

    const indexBuffer = buffer.index_buffer();
    
    for (let i = 0; i < vertexBuffer.length; i+=8) {
      const j = i/8;

      positionBuffer[(j * positionComponentSize)] = vertexBuffer[i];
      positionBuffer[(j * positionComponentSize)+1] = vertexBuffer[i+1];
      positionBuffer[(j * positionComponentSize)+2] = vertexBuffer[i+2];

      uvBuffer[(j * uvComponentSize)] = vertexBuffer[i+3];
      uvBuffer[(j * uvComponentSize)+1] = vertexBuffer[i+4];
      
      normalBuffer[(j * normalComponentSize)] = vertexBuffer[i+5];
      normalBuffer[(j * normalComponentSize)+1] = vertexBuffer[i+6];
      normalBuffer[(j * normalComponentSize)+2] = vertexBuffer[i+7];
    }
    
    const geometry = new THREE.BufferGeometry();
    geometry.setIndex(new THREE.BufferAttribute(indexBuffer, 1));
    geometry.setAttribute("position", new THREE.BufferAttribute(positionBuffer, 3));
    geometry.setAttribute("normal", new THREE.BufferAttribute(normalBuffer, 3));
    geometry.setAttribute("uv", new THREE.BufferAttribute(uvBuffer, 2));

    return [geometry];
  }

  createGeometryPerFace(solid: Truck.Solid): THREE.BufferGeometry[] {
    let geometries: Array<THREE.BufferGeometry> = [];
    const faces = solid.faces();
    for (let face of faces) {
      const shell = face.extract();

      let polygons: Truck.PolygonMesh = shell.to_polygon(0.01);
      let buffer = polygons.to_buffer();
  
      const vertexBuffer = buffer.vertex_buffer();
  
      const positionComponentSize = 3;
      const normalComponentSize = 3;
      const uvComponentSize = 2;
  
      let vertexCount = vertexBuffer.length / (positionComponentSize + normalComponentSize + uvComponentSize);
      
      const positionBuffer = new Float32Array(vertexCount * positionComponentSize);
      const uvBuffer = new Float32Array(vertexCount * uvComponentSize);
      const normalBuffer = new Float32Array(vertexCount * normalComponentSize);
  
      const indexBuffer = buffer.index_buffer();
      
      for (let i = 0; i < vertexBuffer.length; i+=8) {
        const j = i/8;
  
        positionBuffer[(j * positionComponentSize)] = vertexBuffer[i];
        positionBuffer[(j * positionComponentSize)+1] = vertexBuffer[i+1];
        positionBuffer[(j * positionComponentSize)+2] = vertexBuffer[i+2];
  
        uvBuffer[(j * uvComponentSize)] = vertexBuffer[i+3];
        uvBuffer[(j * uvComponentSize)+1] = vertexBuffer[i+4];
        
        normalBuffer[(j * normalComponentSize)] = vertexBuffer[i+5];
        normalBuffer[(j * normalComponentSize)+1] = vertexBuffer[i+6];
        normalBuffer[(j * normalComponentSize)+2] = vertexBuffer[i+7];
      }
      
      const geometry = new THREE.BufferGeometry();
      geometry.setIndex(new THREE.BufferAttribute(indexBuffer, 1));
      geometry.setAttribute("position", new THREE.BufferAttribute(positionBuffer, 3));
      geometry.setAttribute("normal", new THREE.BufferAttribute(normalBuffer, 3));
      geometry.setAttribute("uv", new THREE.BufferAttribute(uvBuffer, 2));

      geometries.push(geometry);
    }

    return geometries;
  }
  
  animate() {
    this.controls.update();
    this.renderer.render(this.scene, this.camera);

    requestAnimationFrame(() => this.animate());
  }

  async createScene(): Promise<THREE.Scene> {
    this.scene = new THREE.Scene();
    const scene = this.scene;

    scene.background = new THREE.Color("#717171");

    const partGroup = new THREE.Group();
    partGroup.name = "part";
    partGroup.scale.set(this.factor/10, this.factor/10, this.factor/10);
    scene.add(partGroup);

    const objectlight = new THREE.PointLight(0xffffff, 2000, 10000, 2);
    objectlight.position.y = 70;
    objectlight.castShadow = true;
    scene.add(objectlight);

    const ambientLight = new THREE.AmbientLight(0x404040);
    ambientLight.intensity = 10;
    scene.add(ambientLight);

    await this.createFloor(1000);

    const wcs = await this.createCSYS();
    scene.add(wcs);

    return scene;
  }

  async createFloor(w: number) {
    const scene = this.scene;

    const geometry = new THREE.PlaneGeometry(w, w, 32);
    const material = new THREE.MeshPhongMaterial({
      // color: 0x606060,
      color: 0xffffff,
      // side: THREE.DoubleSide,
    });
    const mesh = new THREE.Mesh(geometry, material);
    mesh.receiveShadow = true;
    mesh.rotation.x = -Math.PI / 2;
    mesh.name = "floor";
    scene.add(mesh);
  }

  async createCSYS() {
    // draw WCS
    // const wcs = await api.getWorkCoordSystem(part);
    // const wcsx = await api.getWorkAxis(part, wcs, "WCSX");
    // const wcsy = await api.getWorkAxis(part, wcs, "WCSY");
    // const wcsz = await api.getWorkAxis(part, wcs, "WCSZ");
  
    const csysGroup = new THREE.Group();
    csysGroup.name = "csys";
  
    const origin = new THREE.Vector3(0, 0, 0);
    // const x = new THREE.Vector3(wcsx.direction.x, wcsx.direction.y, wcsx.direction.z);
    // const y = new THREE.Vector3(wcsy.direction.x, wcsy.direction.y, wcsy.direction.z);
    // const z = new THREE.Vector3(wcsz.direction.x, wcsz.direction.y, wcsz.direction.z);
  
    const x = new THREE.Vector3(10, 0 ,0);
    const y = new THREE.Vector3(0, 10 ,0);
    const z = new THREE.Vector3(0, 0 ,10);
    
    const xline = this.createLine(origin, x, 0xff0000);
    const yline = this.createLine(origin, y, 0x00ff00);
    const zline = this.createLine(origin, z, 0x0000ff);
    csysGroup.add(xline);
    csysGroup.add(yline);
    csysGroup.add(zline);
  
    // draw WCS origin
    const sphereGeometry = new THREE.SphereGeometry(0.5, 32, 32);
    const sphereMaterial = new THREE.MeshBasicMaterial({ color: 0xffffff });
    const sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
    csysGroup.add(sphere);
  
    // // draw WCS labels
    // const loader = new FontLoader();
    // const font = await loader.loadAsync(helvetiker);
  
    // const textGeometryX = new TextGeometry("X", {
    //   font: font,
    //   size: 2,
    //   height: 0,
    // });
    // const textMaterialX = new THREE.MeshBasicMaterial({ 
    //   color: 0xff0000,
    //   depthTest: true,
    //   depthWrite: true,
    // });
    // const textX = new THREE.Mesh(textGeometryX, textMaterialX);
    // textX.renderOrder = 2;
    // textGeometryX.computeBoundingBox()
    // textX.position.set(x.x + 1, x.y, x.z);
    // textX.lookAt(camera.position)
    // csysGroup.add(textX);
  
    // const textGeometryY = new TextGeometry("Y", {
    //   font: font,
    //   size: 2,
    //   height: 0,
    // });
    // const textMaterial2 = new THREE.MeshBasicMaterial({ 
    //   color: 0x00ff00,
    //   depthTest: true,
    //   depthWrite: true,
    // });
    // const textY = new THREE.Mesh(textGeometryY, textMaterial2);
    // textY.renderOrder = 2;
    // textY.position.set(y.x, y.y + 1, y.z);
    // csysGroup.add(textY);
    // textY.lookAt(camera.position)
  
    // const textGeometryZ = new TextGeometry("Z", {
    //   font: font,
    //   size: 2,
    //   height: 0,
    // });
    // const textMaterialZ = new THREE.MeshBasicMaterial({ 
    //   color: 0x0000ff,
    //   depthTest: true,
    //   depthWrite: true,
    // });
    // const textZ = new THREE.Mesh(textGeometryZ, textMaterialZ);
    // textZ.renderOrder = 2;
    // textZ.position.set(z.x - 1, z.y, z.z + 2);
    // csysGroup.add(textZ);
    // textZ.lookAt(camera.position)
  
    return csysGroup;
  }

  createLine(
    position: THREE.Vector3,
    anchor: THREE.Vector3,
    color: number = 0x000000
  ) {
    let points = [];
    points.push(position);
    points.push(anchor);
    let geometry = new THREE.BufferGeometry().setFromPoints(points);
    let material = new THREE.LineBasicMaterial({ color: color });
    let line = new THREE.Line(geometry, material);
    return line;
  }

}
